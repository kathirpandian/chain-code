// export {default as HomeComponent} from  './home'
export {default as Logo} from  './logo';
export {default as HomeComponent} from  './home/Home';
export {default as AboutUsComponent} from  './about/about-us';
export {default as NFTSComponent} from './service/NftService';
export {default as DefiServiceComponent} from './service/DefiService';
export {default as ContactComponent} from './contact';
export {default as CareersComponent} from './careers';
export {default as CareerDetailsComponent} from './careers/CareerDetails';
export {default as Layout} from  './layout';
