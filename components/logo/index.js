import React from "react";

import { Logo } from "@components/icons";

export default function ChaincodeLogo() {
    return (<Logo color="white" width="96" height="58" />);
};
